class AreasController < ApplicationController
  before_action :require_admin

  def index
    @areas = Area.all.order(id: :desc).paginate(page: params[:page], per_page: 10)
  end

  def show
    @area = Area.find(params[:id])
  end

  def new
    @area = Area.new
  end

  def create
    @area = Area.new(area_params)
    if @area.save
      flash[:success] = "Você criou a área com sucesso!"
      redirect_to areas_path
    else
      render 'new'
    end
  end

  def edit
    @area = Area.find(params[:id])
  end

  def update
    @area = Area.find(params[:id])
    if @area.update(area_params)
      flash[:success] = "Você editou a área com sucesso!"
      redirect_to areas_path
    else
      render 'edit'
    end
  end

  def destroy
    @area = Area.find(params[:id])
    if @area.destroy
      flash[:danger] = "Você excluiu a área com sucesso!"
      redirect_to areas_path
    end
  end

  private
    def area_params
      params.require(:area).permit(:nome)
    end

    def require_admin
      if !current_user.admin?
        flash[:danger] = "Somente administradores podem acessar esta área"
        redirect_to orders_path
      end
    end
end
