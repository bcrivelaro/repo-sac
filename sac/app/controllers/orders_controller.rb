class OrdersController < ApplicationController
  before_action :require_admin, only: [:destroy_old_data]
  before_action :require_representante, only: [:tem_que_pedir, :nao_tem_que_pedir, :tornar_devolvida, :tornar_nao_devolvida]
  before_action :require_same_user, only: [:show]

  def index
    if !current_user.admin? && !current_user.representante?
      @q = Order.all.where(:user => current_user).order(id: :desc).search(params[:q])
      @orders = @q.result.paginate(page: params[:page], per_page: 10)
    end
    if current_user.admin?
      @q = Order.all.order(id: :desc).search(params[:q])
      @orders = @q.result.paginate(page: params[:page], per_page: 10)
    end
    if !current_user.admin? && current_user.representante?
      @q = Order.all.where(:representante => current_user.nome).order(id: :desc).search(params[:q])
      @orders = @q.result.paginate(page: params[:page], per_page: 10)
    end
  end

  def new
    @order = Order.new
  end

  def edit
    @order = Order.find(params[:id])
  end

  def create
    @order = Order.new(order_params)
    @order.user = current_user
    if @order.save
      flash[:success] = "Você criou a solicitção com sucesso!"
      redirect_to @order
    else
      render 'new'
    end
  end

  def update
    @order = Order.find(params[:id])
    if @order.update(order_params)
      flash[:success] = "Você atualizou a solicitção com sucesso!"
      redirect_to orders_path(@order)
    else
      render 'edit'
    end
  end

  def show
    @order = Order.find(params[:id])
    @commentable = @order
    @comments = @commentable.comments
    @comment = Comment.new
    if current_user.nome == @order.representante and !@order.read
      flash[:success] = "Você leu a solicitação!"
      @order.read = true
      @order.save
    end
    require 'correios-sro-xml'
    sro = Correios::SRO::Tracker.new(:user => "ECT", :password => "SRO")
    sro.result_mode = :all
    if @order.rastreio.present?
      @object = sro.get(@order.rastreio)
    end
  end

  def destroy
    @order = Order.find(params[:id])
    if @order.destroy
      redirect_to orders_path
      flash[:danger] = "Você excluiu a solicitação com sucesso!"
    end
  end

  def destroy_old_data
    Order.where("created_at < ?", 360.days.ago).destroy_all
    respond_to do |format|
      format.html { redirect_to orders_path, notice: 'Solicitações antigas foram apagadas com sucesso!' }
      format.json { head :no_content }
    end
  end

  def tornar_devolvida
    @order = Order.find(params[:id])
    @order.marcar_devolvida
    flash[:success] = "Você marcou a solicitação como devolvida com sucesso!"
    redirect_to @order
  end

  def tornar_nao_devolvida
    @order = Order.find(params[:id])
    @order.marcar_nao_devolvida
    flash[:danger] = "Você marcou a solicitação como NÃO devolvida com sucesso!"
    redirect_to @order
  end

  def tem_que_pedir
    @order = Order.find(params[:id])
    @order.marcar_pedir
    flash[:success] = "Você marcou a solicitação para pedir!"
    redirect_to @order
  end

  def nao_tem_que_pedir
    @order = Order.find(params[:id])
    @order.marcar_nao_pedir
    flash[:danger] = "Você marcou a solicitação para NÃO pedir!"
    redirect_to @order
  end

  private
    def order_params
      params.require(:order).permit(:referencia, :motivo, :estado, :imagem, :rastreio, :representante, :read, :devolvida, :pedir)
    end

    def require_admin
      if !current_user.admin?
        flash[:danger] = "Somente administradores podem acessar esta área"
        redirect_to orders_path
      end
    end

    def require_representante
      if !current_user.representante?
        flash[:danger] = "Somente representantes podem acessar esta área"
        redirect_to orders_path
      end
    end

    def require_same_user
      @order = Order.find(params[:id])
      if !current_user.admin? && current_user.nome != @order.representante && current_user != @order.user
        flash[:danger] = "Você não pode acessar esta área"
        redirect_to orders_path
      end
    end

end
