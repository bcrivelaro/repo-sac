class UsersController < ApplicationController
  before_action :require_admin
  before_action :set_user

  def show

  end

  def tornar_admin
    @user.make_admin
    flash[:success] = "Você o tornou administrador com sucesso!"
    redirect_to @user
  end

  def tornar_representante
    @user.make_representante
    flash[:success] = "Você o tornou representante com sucesso!"
    redirect_to @user
  end

  def tirar_admin
    @user.not_admin
    flash[:danger] = "Você o tirou administrador com sucesso!"
    redirect_to @user
  end

  def tirar_representante
    @user.not_representante
    flash[:danger] = "Você o tirou representante com sucesso!"
    redirect_to @user
  end

  private
    def user_params
        params.require(:user).permit(:id)
      end

    def require_admin
      if !current_user.admin?
        flash[:danger] = "Somente administradores podem acessar esta área"
        redirect_to orders_path
      end
    end

    def set_user
      @user = User.find(params[:id])
    end
end
