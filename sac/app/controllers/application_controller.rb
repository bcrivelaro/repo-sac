class ApplicationController < ActionController::Base
  before_action :authenticate_user!
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :configure_permitted_parameters, if: :devise_controller?

  protected
  def configure_permitted_parameters
  devise_parameter_sanitizer.permit(:sign_up) do |user_params|
    user_params.permit(:email, :password, :password_confirmation, :nome, :cnpj, :endereco_rua, :endereco_numero,
                        :endereco_bairro, :endereco_cep, :endereco_cidade, :endereco_estado)
  end
  devise_parameter_sanitizer.permit(:account_update) do |user_params|
    user_params.permit(:email, :password, :password_confirmation, :current_password, :nome, :cnpj, :endereco_rua, :endereco_numero,
                        :endereco_bairro, :endereco_cep, :endereco_cidade, :endereco_estado)
  end
end
end
