class ChamadosController < ApplicationController

  before_action :require_same_user, only: [:show]

  def index
    if !current_user.admin? && !current_user.representante?
      @chamados = Chamado.all.where(:user => current_user).order(id: :desc).paginate(page: params[:page], per_page: 10)
    else
      @chamados = Chamado.all.order(id: :desc).paginate(page: params[:page], per_page: 10)
    end
  end

  def show
    @chamado = Chamado.find(params[:id])
    @commentable = @chamado
    @comments = @commentable.comments
    @comment = Comment.new
    if current_user.admin? and !@chamado.read
      flash[:success] = "Você leu o chamado!"
      @chamado.read = true
      @chamado.save
    end
  end

  def new
    @chamado = Chamado.new
  end

  def create
    @chamado = Chamado.new(chamado_params)
    @chamado.user = current_user
    if @chamado.save
      flash[:success] = "Você abriu o chamado com sucesso!"
      redirect_to chamados_path
    else
      render 'new'
    end
  end

  def edit
    @chamado = Chamado.find(params[:id])
  end

  def update
    @chamado = Chamado.find(params[:id])
    if @chamado.update(chamado_params)
      flash[:success] = "Você atualizou o chamado com sucesso!"
      redirect_to chamados_path(@chamado)
    else
      render 'edit'
    end
  end

  def destroy
    @chamado = Chamado.find(params[:id])
    if @chamado.destroy
      flash[:danger] = "Você excluiu o chamado com sucesso!"
      redirect_to chamados_path
    end
  end

  private
    def chamado_params
      params.require(:chamado).permit(:area, :assunto, :descricao, :estado)
    end

    def require_same_user
      @chamado = Chamado.find(params[:id])
      if !current_user.admin? && current_user != @chamado.user
        flash[:danger] = "Você não pode acessar esta área"
        redirect_to orders_path
      end
    end
end
