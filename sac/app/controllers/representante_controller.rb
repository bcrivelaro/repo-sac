class RepresentanteController < ApplicationController
  before_action :require_representante

  def index
    @q = Order.all.where(:representante => current_user.nome, :read => false).order(id: :desc).search(params[:q])
    @orders = @q.result.paginate(page: params[:page], per_page: 10)
  end

  private

  def require_representante
    if !current_user.representante?
      flash[:danger] = "Somente representantes podem acessar esta área"
      redirect_to orders_path
    end
  end

end
