class AdminController < ApplicationController
  before_action :require_admin

  def index
    @q = Order.all.where(:read => false).order(id: :desc).search(params[:q])
    @orders = @q.result.paginate(page: params[:page], per_page: 10)
  end

  def users
    @users = User.all.order(id: :desc).paginate(page: params[:page], per_page: 25)
  end

  def chamados
    @chamados = Chamado.all.where(:read => false).order(id: :desc).paginate(page: params[:page], per_page: 10)
  end

  def comentarios
    @comments = Comment.all.order(id: :desc).paginate(page: params[:page], per_page: 25)
  end

  def pedir
    @q = Order.all.where(:pedir => true, :estado => "Solicitado").order(id: :desc).search(params[:q])
    @orders = @q.result.paginate(page: params[:page], per_page: 25)
  end

  private

  def require_admin
    if !current_user.admin?
      flash[:danger] = "Somente administradores podem acessar esta área"
      redirect_to orders_path
    end
  end

end
