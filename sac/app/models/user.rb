class User < ActiveRecord::Base
  has_many :orders
  has_many :chamados
  has_many :comments
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  VALID_EMAIL_REGEX = /\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i

  validates :email, presence: true, length: {maximum: 105},
            uniqueness: {case_sensitive: false},
            format: {with: VALID_EMAIL_REGEX}

  validates :nome, presence: true
  validates :endereco_cep, presence: true
  validates :endereco_rua, presence: true
  validates :endereco_numero, presence: true
  validates :endereco_bairro, presence: true
  validates :endereco_cidade, presence: true
  validates :endereco_estado, presence: true

  def make_admin
    self.update_column(:admin, true)
  end

  def make_representante
    self.update_column(:representante, true)
  end

  def not_admin
    self.update_column(:admin, false)
  end

  def not_representante
    self.update_column(:representante, false)
  end
end
