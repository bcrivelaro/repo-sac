class Order < ActiveRecord::Base
  belongs_to :user
  has_many :comments, as: :commentable
  validates :referencia, presence: true, length: {minimum: 5}
  validates :motivo, presence: true, length: {minimum: 4}
  validates :representante, presence: true
  mount_uploader :imagem, ImagemUploader

  def marcar_devolvida
    self.update_column(:devolvida, true)
  end

  def marcar_nao_devolvida
    self.update_column(:devolvida, false)
  end

  def marcar_pedir
    self.update_column(:pedir, true)
  end

  def marcar_nao_pedir
    self.update_column(:pedir, false)
  end

end
