class Chamado < ActiveRecord::Base
  belongs_to :user
  has_many :comments, as: :commentable

  validates :assunto, presence: true
  validates :descricao, presence: true
end
