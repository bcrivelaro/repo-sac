Rails.application.routes.draw do

  devise_for :users
  resources :users, only: [:show] do
    collection do
      put 'users/:id' => 'users#tornar_admin', :as => "tornar_admin"
      put 'users2/:id' => 'users#tirar_admin', :as => "tirar_admin"
      put 'users3/:id' => 'users#tornar_representante', :as => "tornar_representante"
      put 'users4/:id' => 'users#tirar_representante', :as => "tirar_representante"
    end
  end

  resources :orders do
    collection do
      delete 'destroy_old_data'
      put 'orders/:id' => 'orders#tornar_devolvida', :as => "tornar_devolvida"
      put 'orders2/:id' => 'orders#tornar_nao_devolvida', :as => "tornar_nao_devolvida"
      put 'orders3/:id' => 'orders#tem_que_pedir', :as => "tem_que_pedir"
      put 'orders4/:id' => 'orders#nao_tem_que_pedir', :as => "nao_tem_que_pedir"
    end
    resources :comments
  end

  resources :chamados do
    resources :comments
  end

  resources :areas
  get 'welcome/index'
  get 'admin/index'
  get 'admin/users'
  get 'admin/chamados'
  get 'admin/comentarios'
  get 'admin/pedir'

  get 'representante/index'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
