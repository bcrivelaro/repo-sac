class CreateGarantia < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :referencia
      t.string :motivo
      t.string :estado, default: "Solicitado"
      t.string :imagem
      t.string :rastreio

      t.timestamps null: false
    end
  end
end
