class AddPedirToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :pedir, :boolean, default: false
  end
end
