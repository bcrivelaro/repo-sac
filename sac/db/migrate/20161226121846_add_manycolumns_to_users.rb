class AddManycolumnsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :endereco_rua, :string
    add_column :users, :endereco_numero, :string
    add_column :users, :endereco_bairro, :string
    add_column :users, :endereco_cep, :string
    add_column :users, :endereco_cidade, :string
    add_column :users, :endereco_estado, :string
  end
end
