class AddEstadoToChamados < ActiveRecord::Migration
  def change
    add_column :chamados, :estado, :string, default: "Aberto"
  end
end
