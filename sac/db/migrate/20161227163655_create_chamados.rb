class CreateChamados < ActiveRecord::Migration
  def change
    create_table :chamados do |t|
      t.integer :user_id
      t.string :area
      t.string :assunto
      t.text :descricao
      t.timestamps null: false
    end
  end
end
