class AddReadToChamados < ActiveRecord::Migration
  def change
    add_column :chamados, :read, :boolean, default: false
  end
end
