class AddDevolvidaToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :devolvida, :boolean, default: false
  end
end
