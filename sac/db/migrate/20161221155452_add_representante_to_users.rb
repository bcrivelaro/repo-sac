class AddRepresentanteToUsers < ActiveRecord::Migration
  def change
    add_column :users, :representante, :boolean, default: false
  end
end
