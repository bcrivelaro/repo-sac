# Projeto #

* Projeto feito para a distribuidora de óculos da marca Paulo Carraro.
* O objetivo deste projeto é centralizar os pedidos de garantias em um lugar só, além de oferecer um maior controle para o representante e os clientes de todas as garantias.
* O sistema criado oferece:
* - Usuários normais podem solicitar garantias, descrevendo o defeito da mesma, e se possível, atrelar uma imagem do defeito do produto para maior controle da distribuidora.
* - Usuários normais podem abrir chamados para resolverem problemas como títulos atrasados, divergencia de entrega de produtos, etc.
* - Garantias e chamados podem ser comentados para uma melhor comunicação entre o cliente e o representante/distribuidora.
* - Garantias podem ser lidas apenas pelos representantes.
* - Chamados podem ser lidos apenas pelos administradores.
* - Garantias podem ter seu estado mudado por seus representante ou por administradores.
* - Usuários representantes podem listar todas as garantias de seus clientes, podendo marcá-las como devolvidas ou não para seu próprio controle, além de poder marcar as garantias para pedir para fabricar ou não.
* - Usuários administradores podem listar todas as garantias e chamados do sistema.
* - Usuários administradores podem também marcar as garantias para serem pedidas ou não, como devolvidas ou não.
* - Usuários administradores podem tornar os outros usuários como representantes ou administradores.
* - Usuários administradores e representantes possuem cores de comentários diferentes nas garantias e chamados.
* - Usuários administradores podem criar áreas para os chamados.
* - Usuários administradores podem deletar garantias antigas para não sobrecarregar o sistema.

# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact